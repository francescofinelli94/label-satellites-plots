# coding: utf-8
"""
collects all the new_labels files and generate results
"""

from os.path import basename as os_path_basename
from os.path import join as os_path_join
from os import getcwd as os_pwd
from os import chdir as os_cd
from glob import glob

from pandas import DataFrame as pd_DataFrame
from pandas import read_csv as pd_read_csv

import file_handler as fh

UNCLEAR_MARGIN = 0.05

os_cd(os_path_join('..', 'collected_files'))
COL_DIR = os_pwd()  # directories containing labels_files
os_cd(os_path_join('..', 'scripts'))

dirs, files, meta_tree = fh.init_file_handler()
files = sorted(files)

data = {}
for f in files:
    fname = os_path_basename(f)
    data[fname] = {}
    meta = fh.file2meta(f, dirs['rec'], dirs['non_rec'])
    data[fname]['old_label'] = meta[0]
    data[fname]['rec'] = 0
    data[fname]['non_rec'] = 0
    data[fname]['unclear'] = 0

labfiles = glob(os_path_join(COL_DIR, 'new_labels_*.dat'))
for labfile in labfiles:
    df = pd_read_csv(labfile, header=None, names=['file_name', 'label'])
    for _, row in df.iterrows():
        data[row['file_name']][row['label']] += 1

data4df = {'file_name': [],
           'old_label': [],
           'rec': [],
           'non_rec': [],
           'unclear': [],
           'score': [],
           'new_label': []}

for fname in data:
    data4df['file_name'].append(fname)
    for k in data[fname]:
        data4df[k].append(data[fname][k])
    data4df['score'].append(((1.*(data[fname]['rec'] - data[fname]['non_rec'])) /
                             (1.*(data[fname]['rec'] + data[fname]['non_rec'] +
                              data[fname]['unclear']))))
    if data4df['score'][-1] > UNCLEAR_MARGIN:
        data4df['new_label'].append('rec')
    elif data4df['score'][-1] < -UNCLEAR_MARGIN:
        data4df['new_label'].append('non_rec')
    else:
        data4df['new_label'].append('unclear')

df = pd_DataFrame(data4df)
df.to_csv(os_path_join(COL_DIR, 'labeling_results.csv'), header=True, index=False)

print('')
print('Data collection and processing done!')
print('Check the file:')
print(os_path_join(COL_DIR, 'labeling_results.csv'))
print('')

# OLD       NEW     CLASS
# rec       rec     RR
# rec       non_rec RN
# rec       unclear RU
# non_rec   rec     NR
# non_rec   non_rec NN
# non_rec   unclear NU

partition = {'RR':[], 'RN':[], 'RU':[],
             'NR':[], 'NN':[], 'NU':[],
             'UR':[], 'UN':[], 'UU':[]}

for _, row in df.iterrows():
    old = row['old_label']
    new = row['new_label']
    if old == 'rec':
        a = 'R'
    elif old == 'non_rec':
        a = 'N'
    else:
        a = 'U'
    if new == 'rec':
        b = 'R'
    elif new == 'non_rec':
        b = 'N'
    else:
        b = 'U'
    partition['{}{}'.format(a, b)].append(row['file_name'])

print('')
print('-----------------------------------')
print('OLD LABEL\tNEW LABEL\tTOT')
print('rec\t\trec\t\t{}'.format(len(partition['RR'])))
print('rec\t\tnon_rec\t\t{}'.format(len(partition['RN'])))
print('rec\t\tunclear\t\t{}'.format(len(partition['RU'])))
print('non_rec\t\trec\t\t{}'.format(len(partition['NR'])))
print('non_rec\t\tnon_rec\t\t{}'.format(len(partition['NN'])))
print('non_rec\t\tunclear\t\t{}'.format(len(partition['NU'])))
print('unclear\t\trec\t\t{}'.format(len(partition['UR'])))
print('unclear\t\tnon_rec\t\t{}'.format(len(partition['UN'])))
print('unclear\t\tunclear\t\t{}'.format(len(partition['UU'])))
print('-----------------------------------')
print('')

for part in partition:
    if len(partition[part]) == 0:
        continue
    with open(os_path_join(COL_DIR, '{}_filenames.dat'.format(part)), 'w') as f:
        for filename in partition[part]:
            f.write('{}\n'.format(filename))

print('One file for each row of this table is created, containing the relative filenames.')

print('')
print('DONE!')
