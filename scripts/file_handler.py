# coding: utf-8
"""
handles files, generating meetadata from file names and vicerversa
"""

from glob import glob
from os.path import join as os_path_join
from os import getcwd as os_pwd
from os import chdir as os_cd

os_cd(os_path_join('..', 'output_data'))
WORK_DIR = os_pwd()  # directories containing files are here
os_cd(os_path_join('..', 'scripts'))


def init_file_handler(work_dir=WORK_DIR):
    """
    init the file handling process
    """
    dirs = get_dirs(work_dir)
    files = get_files(dirs)
    meta_tree = make_meta_tree(files, dirs)
    return dirs, files, meta_tree


def get_dirs(work_dir=WORK_DIR):
    """
    get the directories into which the files are subdivided
    """
    dirs = {}
    dirs['rec'] = glob(os_path_join(work_dir, 'rec*', ''))[0]
    dirs['non_rec'] = glob(os_path_join(work_dir, 'non_rec*', ''))[0]
    return dirs


def get_files(dirs):
    """
    get the names of all the files
    """
    files = (glob(os_path_join(dirs['rec'], '*.txt')) +
             glob(os_path_join(dirs['non_rec'], '*.txt')))
    return files


def file2meta(file_path, dir_rec='reconnection',
              dir_non_rec='non_reconnection'):
    """
    generate complete metadata out of a filename
    """
    string = file_path.split('case_resampled')[1][:-4]
    day, string = string.split('T')
    hour = string.split('_')[0]
    probe = string.split('_')[-1]
    res = string.split(hour+'_')[-1].split('_'+probe)[0]
    if dir_rec in file_path:
        label = 'rec'
    elif dir_non_rec in file_path:
        label = 'non_rec'
    else:
        label = 'UNKNOWN'
    return label, day, hour, probe, res


def meta2file(meta, dir_rec='reconnection', dir_non_rec='non_reconnection'):
    """
    generate a file name out of complete metadata
    """
    label, day, hour, probe, res = meta
    file_path = 'case_resampled{}T{}_{}_{}.txt'.format(day, hour, res, probe)
    if label == 'rec':
        file_path = os_path_join(dir_rec, file_path)
    elif label == 'non_rec':
        file_path = os_path_join(dir_non_rec, file_path)
    else:
        print('label is {}, metadata connot be linjed to a directory!'.format(label))
    return file_path


def make_meta_tree(files, dirs):
    """
    generate a tree-sctructure of metadata (as nested dictionaries)
    """
    meta_tree = {}
    for f in files:
        label, day, hour, probe, res = file2meta(f, dirs['rec'],
                                                 dirs['non_rec'])
        if label not in meta_tree:
            meta_tree[label] = {}
        if day not in meta_tree[label]:
            meta_tree[label][day] = {}
        if hour not in meta_tree[label][day]:
            meta_tree[label][day][hour] = {}
        if probe not in meta_tree[label][day][hour]:
            meta_tree[label][day][hour][probe] = {}
        if res not in meta_tree[label][day][hour][probe]:
            meta_tree[label][day][hour][probe][res] = {}
    return meta_tree


def navigate_meta_tree(meta_tree, branch=None, pos=[]):
    """
    a "file explorer" to navigate the files and then selec one,
    retrieving its metadata (o retrieve partial metadata, corresponding to
    multiple files)
    """
    if branch is None:
        branch = meta_tree
    print('')
    print('-----------------------')
    print('Position: / {}'.format(' / '.join(pos)))
    opt = []
    i = -1
    for i, k in enumerate(sorted(branch.keys())):
        print('{} -> {}'.format(i, k))
        opt.append(i)
    i += 1
    print('{} -> {}'.format(i, 'done!'))
    opt.append(i)
    i += 1
    print('{} -> {}'.format(i, 'go back!'))
    opt.append(i)
    incorrect = True
    while incorrect:
        ans = input('Insert a number: ')
        try:
            ans = int(ans)
        except ValueError as val_err:
            print(val_err)
            print('Thi is not an integer! Retry.')
            continue
        if ans not in opt:
            print('{} is not an option, retry.'.format(ans))
        else:
            incorrect = False
    if ans == opt[-2]:
        return tuple(pos)
    if ans == opt[-1] and len(pos) > 0:
        _ = pos.pop()
        branch = meta_tree
        for p in pos:
            branch = branch[p]
    elif ans in opt[:-2]:
        k = sorted(branch.keys())[ans]
        pos = pos + [k]
        branch = branch[k]
    return navigate_meta_tree(meta_tree, branch=branch, pos=pos)


def main():
    """
    just a main for a test run
    """
    dirs = get_dirs()
    files = get_files(dirs)

    meta0 = file2meta(files[0], dirs['rec'], dirs['non_rec'])
    file0 = meta2file(meta0, dirs['rec'], dirs['non_rec'])
    print('files[0] = {}'.format(files[0]))
    print('meta0 = {}'.format(meta0))
    print('file0 = {}'.format(file0))
    print('file0 == files[0] = {}'.format(file0 == files[0]))

    print('')

    meta1 = file2meta(files[-1], dirs['rec'], dirs['non_rec'])
    file1 = meta2file(meta1, dirs['rec'], dirs['non_rec'])
    print('files[-1] = {}'.format(files[-1]))
    print('meta1 = {}'.format(meta1))
    print('file1 = {}'.format(file1))
    print('file1 == files[-1] = {}'.format(file1 == files[-1]))

    meta_tree = make_meta_tree(files, dirs)
    pos = navigate_meta_tree(meta_tree)
    print(pos)
    file2 = meta2file(pos, dirs['rec'], dirs['non_rec'])
    print(file2)


if __name__ == '__main__':
    main()
