# coding: utf-8
"""
let a human lable the different files
"""

from sys import exit as sys_exit
from glob import glob
from os.path import join as os_path_join
from os.path import basename as os_path_basename
from os import getcwd as os_pwd
from os import chdir as os_cd
from random import shuffle as rnd_shuffle

from pandas import read_csv as pd_read_csv

import file_handler as fh
import make_plots as mp

# FLG_SAVE = True
FLG_SAVE = False

os_cd(os_path_join('..', 'label_files'))
LAB_DIR = os_pwd()  # directories containing labels_file
os_cd(os_path_join('..', 'scripts'))

dirs, files, meta_tree = fh.init_file_handler()

labfiles = sorted(glob(os_path_join(LAB_DIR, 'new_labels_*.dat')))

opt = []
basenames = []
i = -1
for i, lf in enumerate(labfiles):
    lf = os_path_basename(lf)
    print('{} -> {}'.format(i, lf))
    opt.append(i)
    basenames.append(lf)
i += 1
print('{} -> {}'.format(i, 'make a new one!'))
opt.append(i)
i += 1
print('{} -> {}'.format(i, 'exit!'))
opt.append(i)

print('')
print('Choose a file to continue labeling or start a new one.')
incorrect = True
while incorrect:
    ans = input('Insert a number: ')
    try:
        ans = int(ans)
    except ValueError as val_err:
        print(val_err)
        print('Thi is not an integer! Retry.')
        continue
    if ans not in opt:
        print('{} is not an option, retry.'.format(ans))
    else:
        incorrect = False

if ans == opt[-1]:
    sys_exit(0)
if ans == opt[-2]:
    while True:
        lfname = input('Choose new_labels_<THIS PART>.dat of the new file name: ')
        lfname = 'new_labels_{}.dat'.format(lfname)
        if lfname in basenames:
            print('This file already exists! Try another one.')
            continue
        break
    labfile = os_path_join(LAB_DIR, lfname)
    shuffled_files = files.copy()
    rnd_shuffle(shuffled_files)
    shffile = os_path_join(LAB_DIR, '.{}.shufled_files'.format(lfname.split('.')[0]))
    with open(shffile, 'w') as f:
        for file_ in shuffled_files:
            f.write('{}\n'.format(file_))
    with open(labfile, 'w') as f:
        f.write('')
else:
    labfile = labfiles[ans]
    lfname = os_path_basename(labfile)
    shffile = os_path_join(LAB_DIR, '.{}.shufled_files'.format(lfname.split('.')[0]))
    
with open(shffile, 'r') as shffile_pointer:
    shuffled_files = shffile_pointer.readlines()

with open(labfile, 'r') as labfile_pointer:
    files_done = labfile_pointer.readlines()

istart = len(files_done)
for fpath in shuffled_files[istart:]:
    df = pd_read_csv(fpath[:-1])
    df['JE'] = (df['Jfpix']*df['Ex'] + df['Jfpiy']*df['Ey'] +
                df['Jfpiz']*df['Ez'])
    fields = mp.cols2fields(df.columns)
    meta = fh.file2meta(fpath[:-1], dirs['rec'], dirs['non_rec'])
    try:
        fields = [fields[0],
                  fields[1],
                  fields[3],
                  fields[2],
                  fields[4],
                  fields[8],
                  fields[6],
                  fields[5]]  # HARDCODED!
    except IndexError:
        fields = [fields[0],
                  fields[1],
                  fields[3],
                  fields[2],
                  fields[4],
                  fields[7],
                  fields[5]]  # HARDCODED!
    with_magnitude = ['B', 'Jfpi']
    with_zero_line = ['B', 'E', 'Vi', 'Ve', 'Jfpi', 'curl_Ve', 'JE']
    mp.plot_func(df, meta, fields, with_magnitude=with_magnitude,
                 with_zero_line=with_zero_line, flg_save=FLG_SAVE)

    while True:
        label = input('Choose a label between \'rec\', \'non_rec\', and \'unclear\': ')
        if label not in ['rec', 'non_rec', 'unclear']:
            print('Invalid option! Retry.')
            continue
        break
    with open(labfile, 'a') as labfile_pointer:
        labfile_pointer.write('{},{}\n'.format(os_path_basename(fpath[:-1]),
                                               label))

    while True:
        ans = input('Press enter to continue, insert \'exit\' to exit: ')
        if ans not in ['', 'exit']:
            print('Invalid option! Retry.')
            continue
        break
    if ans == '':
        continue
    print('')
    print('-------------------------')
    print('')
    print('Exiting... See you soon!!!')
    sys_exit(0)

print('')
print('-------------------------')
print('')
print('The labeling in done!!!')
print('Send the file')
print('{}'.format(labfile))
print('to francesco.finelli@phd.unipi.it .')
print('Thanks for helping!!!')
