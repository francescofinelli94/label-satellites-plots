# coding: utf-8
"""
makes plots from satellite data
"""

from pandas import read_csv as pd_read_csv
import matplotlib.pyplot as plt
from numpy import sqrt as np_sqrt
from numpy import ndarray as np_ndarray

import file_handler as fh


def cols2fields(cols):
    """
    take a list of columns names and produce a list of "fields".
    each "field" is a list containing the names of the components
    of said field.
    """
    fields_ = []
    for col in cols:
        if col[-1] in ['x', 'y', 'z']:
            if col[:-1] not in fields_:
                fields_.append(col[:-1])
        else:
            fields_.append(col)
    fields = []
    for field in fields_:
        tmp = []
        for ax in ['x', 'y', 'z']:
            if field+ax in cols:
                tmp.append(field+ax)
        if len(tmp) == 0:
            tmp = [field]
        fields.append(tmp)
    return fields


def field_on_axes(ax, field, df, x, zero_line=True, magnitude=True):
    """
    plots a given field on a given axes
    """
    ncomps = len(field)
    if ncomps == 1:
        magnitude = False
    ncol = ncomps
    if magnitude:
        ncol += 1

    if zero_line:
        ax.axhline(y=0., ls='--', c='k')
    for comp in field:
        ax.plot(x, df[comp], lw=.7, label=comp)
    if magnitude:
        ssq = df[field[0]]*df[field[0]]
        for comp in field[1:]:
            ssq = ssq + df[comp]*df[comp]
        ax.plot(x, np_sqrt(ssq), ls='--', label='|{}|'.format(field[0][:-1]))

    ax.legend(ncol=ncol)


def plot_func(df, meta, fields, label_in_title=False, with_magnitude=[],
              with_zero_line=[], flg_show=True, flg_save=False):
    """
    makes the plot
    """
    npts = len(df)
    label, day, hour, probe, res = meta
    if res == 'ion_resolution':
        dx = 0.05  # d_i
        x = [xx*dx for xx in range(npts)]
        xlabel = 's [di]'
    else:
        x = [xx for xx in range(npts)]
        xlabel = 'point index'
    nrow = len(fields)

    plt.close('all')
    fig, axs = plt.subplots(nrow, 1, figsize=(10, 11), sharex=True)
    if not isinstance(axs, np_ndarray):
        axs = [axs]
    plt.subplots_adjust(hspace=0., top=.97, bottom=.05, right=.99, left=.07)
    for ax in axs:
        if ax == axs[-1]:
            ax.tick_params(bottom=True, top=True, left=True, right=True,
                           direction='inout', labelbottom=True, labeltop=False,
                           labelleft=True, labelright=False)
        else:
            ax.tick_params(bottom=True, top=True, left=True, right=True,
                           direction='inout', labelbottom=False,
                           labeltop=False, labelleft=True, labelright=False)

    axs[-1].set_xlabel(xlabel)
    if label_in_title:
        axs[0].set_title('{} {} {} {} {}'.format(label, day, hour, probe, res))
    else:
        axs[0].set_title('{} {} {} {}'.format(day, hour, probe, res))

    for i, field in enumerate(fields):
        if len(field) > 1:
            field_name = field[0][:-1]
        else:
            field_name = field[0]
        if field_name in with_magnitude:
            magnitude = True
        else:
            magnitude = False
        if field_name in with_zero_line:
            zero_line = True
        else:
            zero_line = False
        field_on_axes(axs[i], field, df, x, zero_line=zero_line,
                      magnitude=magnitude)

    if flg_save:
        fig.savefig('../plots/{}_{}_{}_{}_{}.png'.format(label, day, hour, probe, res))
    if flg_show:
        plt.show()
    plt.close()


def main():
    """
    main for a test run
    """
    dirs, files, meta_tree = fh.init_file_handler()

    meta = fh.navigate_meta_tree(meta_tree)
    file_path = fh.meta2file(meta, dirs['rec'], dirs['non_rec'])

    df = pd_read_csv(file_path)
    df['JE'] = (df['Jfpix']*df['Ex'] + df['Jfpiy']*df['Ey'] +
                df['Jfpiz']*df['Ez'])
    fields = cols2fields(df.columns)
    try:
        fields = [fields[0],
                  fields[1],
                  fields[3],
                  fields[2],
                  fields[4],
                  fields[8],
                  fields[6],
                  fields[5]]  # HARDCODED!
    except IndexError:
        fields = [fields[0],
                  fields[1],
                  fields[3],
                  fields[2],
                  fields[4],
                  fields[7],
                  fields[5]]  # HARDCODED!

    with_magnitude = ['B', 'Jfpi']
    with_zero_line = ['B', 'E', 'Vi', 'Ve', 'Jfpi', 'curl_Ve', 'JE']
    plot_func(df, meta, fields, with_magnitude=with_magnitude,
              with_zero_line=with_zero_line, flg_save=True)


if __name__ == '__main__':
    main()
