# coding: utf-8
"""
plots all the files
"""

from pandas import read_csv as pd_read_csv

import file_handler as fh
import make_plots as mp

dirs, files, meta_tree = fh.init_file_handler()

nfiles = len(files)
for i, f in enumerate(files):
    df = pd_read_csv(f)
    df['JE'] = (df['Jfpix']*df['Ex'] + df['Jfpiy']*df['Ey'] +
                df['Jfpiz']*df['Ez'])
    fields = mp.cols2fields(df.columns)
    meta = fh.file2meta(f, dirs['rec'], dirs['non_rec'])
    try:
        fields = [fields[0],
                  fields[1],
                  fields[3],
                  fields[2],
                  fields[4],
                  fields[8],
                  fields[6],
                  fields[5]]  # HARDCODED!
    except IndexError:
        fields = [fields[0],
                  fields[1],
                  fields[3],
                  fields[2],
                  fields[4],
                  fields[7],
                  fields[5]]  # HARDCODED!
    with_magnitude = ['B', 'Jfpi']
    with_zero_line = ['B', 'E', 'Vi', 'Ve', 'Jfpi', 'curl_Ve', 'JE']
    mp.plot_func(df, meta, fields, label_in_title=True,
                 with_magnitude=with_magnitude, with_zero_line=with_zero_line,
                 flg_show=False, flg_save=True)
    print('\r                    ', end='', flush=True)
    print('\rProgress: {}%'.format(round(float(i+1)/float(nfiles)*100., 1)),
          end='', flush=True)

print('')
print('-------------------')
print('')
print('All plots done!!!')
